package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.Random;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    public void run() {
        // TODO: Implement Rock Paper Scissors
        while (true){
            System.out.println("Let's play round " + roundCounter);
            while (true) {
                String humanChoice = readInput("Your choice (Rock/Paper/Scissors)?");
                if (rpsChoices.contains(humanChoice)){
                    Random r = new Random(); //hentet fra https://stackoverflow.com/questions/6726963/random-string-from-string-array-list/6726973
                    int randomitem = r.nextInt(rpsChoices.size());
                    String computerChoice = rpsChoices.get(randomitem);
                    String choiceString = "Human chose "+humanChoice + ", computer chose " + computerChoice + ".";
                    if (isWinner(humanChoice, computerChoice)){
                        System.out.println(choiceString + " Human wins.");
                        humanScore++;
                    }
                    else if (isWinner(computerChoice, humanChoice)){
                        System.out.println(choiceString + " Computer wins.");
                        computerScore++;
                    }
                    else{
                        System.out.println(choiceString + " It's a tie");
                    }
                    System.out.println("Score: human " + humanScore + ", computer " + computerScore);
                    roundCounter++;
                    break;

                }  
                else{
                    System.out.println("I do not understand " + humanChoice + ". Could you try again?");
                }
            }
            String humanResponse = readInput("Do you wish to continue playing? (y/n)?");
            if (humanResponse.equalsIgnoreCase("y")){
                continue;
            }
            else{
                System.out.println("Bye bye :)");
                break;
            }

              

    }
        }
        
    public Boolean isWinner(String choice1, String choice2) { //insirasjon fra pyhton koden
        if (choice1.equals("paper")){
            return choice2.equals("rock");
        }
        else if (choice1.equals("scissors")){
            return choice2.equals("paper");
        }
        else{
            return choice2.equals("scissors");
        }
    }

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}
